all:
	cmake --version
	cmake -B build -G Ninja -S .
	cmake --build build --parallel
	build/pack

entr:
	ls *.cxx Makefile CMakeLists.txt | entr -cr make


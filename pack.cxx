#include <print>

int main() {

  constexpr struct {
  } a;

  constexpr struct {
    char a_{};
    int b_{};
    double c_{};
  } b;

  constexpr struct {
    int b_{};
    double c_{};
    char a_{};
  } c;

#pragma pack(push, 1)
  constexpr struct {
    int b_{};
    double c_{};
    char a_{};
  } d;
#pragma pack(pop)

  constexpr struct {
    int a_ : 2;
    int b_ : 2;
    int c_ : 5;
  } e{0u, 1u, 1u};

#pragma pack(push, 1)
  constexpr struct {
    int a_ : 2;
    int b_ : 2;
    int c_ : 5;
  } f{{}, {}, {}};
#pragma pack(pop)

  static_assert(sizeof a == 1);
  static_assert(sizeof b == 16);
  static_assert(sizeof c == 24);
  static_assert(sizeof d == 13);

  std::println("sizeof a: {}", sizeof a);
  std::println("sizeof b: {}, alignof b: {}", sizeof b, alignof(decltype(b)));
	std::println("sizeof c: {}", sizeof c);
  std::println("sizeof d: {}", sizeof d);
  std::println("sizeof e: {}", sizeof e);
  std::println("sizeof f: {}", sizeof f);
}

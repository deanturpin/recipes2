cmake_minimum_required(VERSION 3.28)
project(recipes VERSION 0.1.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Wpedantic")

add_executable(pack pack.cxx)
